euc_soc_meta_repositories
=============================

This repository is a helper package to allow checking the status of all the
repositories `euc_soc_XXXX`.  Clone it under the same folder as the other
repositories, and launch the script `update-repo-status.sh`.  This will 
generate the file `repositories.status` which contains (in Emacs 
_compilation_output format) the list of modified/added/removed ... files
in all those repositories (including itself).
