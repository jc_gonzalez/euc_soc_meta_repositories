#!/bin/bash
SCRIPTPATH="$(cd "$(dirname "$0")" && pwd)"
SCRIPTNAME="$(basename "$0")"

BASE="$(dirname "$(dirname "${SCRIPTPATH}")")"

( \
  echo '-*- compilation -*-'
  for i in  $(cat repositories); do
      repo="${BASE}/$i"
      cd "${repo}"
      git status --short 1>/tmp/k 2>&1
      k=$(cat /tmp/k)
      if [ -n "$k"  ]; then
          echo; echo "${repo}:0: Check this!"
          cat /tmp/k
      fi
      cd - >/dev/null
  done
) | tee repositories.status



