#!/bin/bash
SCRIPTPATH="$(cd "$(dirname "$0")" && pwd)"
SCRIPTNAME="$(basename "$0")"

BASE="$(dirname "$(dirname "${SCRIPTPATH}")")"
BASEURL="https://jc_gonzalez@bitbucket.org/jc_gonzalez"

for i in  $(cat repositories); do
    repofull="${BASE}/$i"
    folder="$(dirname "${repofull}")"
    repo="$(basename "${repofull}")"
    repourl="${BASEURL}/${repo}.git"

    mkdir -p "${folder}"
    cd "${folder}"
    git clone --recurse-submodules "${repourl}" "${repo}"
    cd - >/dev/null
done
